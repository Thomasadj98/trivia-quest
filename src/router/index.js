import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Home',
        component: () => import(/* webpackChunkName: "gamesettings" */ '../components/Home/Home.vue')
    },
    {
        path: '/questions',
        name: 'Questions',
        component: () => import(/* webpackChunkName: "questions" */ '../components/Questions/Questions.vue')
    },
    {
        path: '/results',
        name: 'Results',
        component: () => import(/* webpackChunkName: "results" */ '../components/Result/Results')
    }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router;