export const TriviaAPI = {
    getQuestions(details) {
        return fetch(`https://opentdb.com/api.php?${details}`)
        .then(response => response.json())
    },

    getCategories() {
        return fetch('https://opentdb.com/api_category.php')
        .then(response => response.json())
        .then(data => data.trivia_categories)
    },

    getToken() {
        return fetch('https://opentdb.com/api_token.php?command=request')
        .then(response => response.json())
    }
}