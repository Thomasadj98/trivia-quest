import Vue from 'vue'
import Vuex from 'vuex'
import { TriviaAPI } from "@/components/Questions/TriviaAPI"
import router from '../router/index'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        selectedCategory: '12',
        categories: ['Loading...'],
        questionIndex: 0,
        questions: [],
        difficulty: 'medium',
        amountOfQuestions: '10',
        error: '',
        token: '',
        isLoading: true
    },
    mutations: {
        setSelectedCategory: (state, payload) => {
            state.selectedCategory = payload
        },
        setCategories: (state, payload) => {
            state.categories = payload
        },
        setQuestions: (state, payload) => {
            state.questions = payload
        },
        setQuestionIndex: (state, payload) => {
            state.questionIndex = payload
        },
        setAnswer: (state, payload) => {
            state.questions[state.questionIndex].answer = payload
        },
        setDifficulty: (state, payload) => {
            state.difficulty = payload
        },
        setAmountOfQuestions: (state, payload) => {
            state.amountOfQuestions = payload
        },
        setError: (state, payload) => {
            state.error = payload
        },
        setToken: (state, payload) => {
          state.token = payload
        },
        setLoading: (state, payload) => {
            state.isLoading = payload
        }
    },
    getters: {
        answers: state => {
            const answers = [...state.questions[state.questionIndex].incorrect_answers, state.questions[state.questionIndex].correct_answer]
            const shuffle = (a) => {
                for (let i = a.length - 1; i > 0; i--) {
                    const j = Math.floor(Math.random() * (i + 1));
                    [a[i], a[j]] = [a[j], a[i]];
                }
                return a;
            }
            return shuffle(answers)
        }
    },
    actions: {

        async getTriviaToken({ commit }) {
            try {
                const response = await TriviaAPI.getToken()

                if(response.response_code === 0) {
                    commit('setToken', response.token)
                } else {
                    commit('setError', `Error getting questions ${response.response_code}`)
                }
            } catch (e) {
                commit('setError', e.message)
            }
        },

        async getTriviaQuestions({ state, commit }) {
            commit('setLoading', true)
            commit('setQuestionIndex', 0)
            commit('setError', '')

            if(state.token === '') {
                await this.dispatch('getTriviaToken');
            }

            try {
                const options = {
                    amount: state.amountOfQuestions,
                    difficulty: state.difficulty,
                    category: state.selectedCategory,
                    token: state.token
                }

                const params = new URLSearchParams(options)
                const response = await TriviaAPI.getQuestions(params)

                switch (response.response_code) {
                    case 0:
                        // Success
                        commit('setQuestions', response.results)
                        break;
                    case 1:
                        // Not enough results
                        commit('setError', 'Not enough questions in database, try a different category.')
                        break;
                    case 2:
                        // Invalid Parameter
                        commit('setError', 'Invalid parameters entered.')
                        break;
                    case 3:
                        // Code 3: Token Not Found
                        commit('setError', 'Please try again.')
                        // Reset token to load it again on next request
                        commit('setToken', '')
                        break;
                    case 4:
                        // Code 4: Token Empty (no new questions left)
                        commit('setError', 'Not enough questions in database, try a different category.')
                        break;
                    default:
                        commit('setError', 'Error getting questions, please try again.')
                        break;
                }
            } catch (e) {
                commit('setError', e.message)
                commit('setLoading', false)
            }
            commit('setLoading', false)
        },

        async getTriviaCategories({ commit }) {
            commit('isLoading', true)
            try {
                const categories = await TriviaAPI.getCategories()

                if(categories) {
                    commit('setCategories', categories)
                } else {
                    commit('setError', 'There are some problems, please try again.')
                }
            } catch (e) {
                commit('setError', e.message)
            }
            commit('setLoading', false)
        },
        goToNextQuestion({ state, commit }) {
            if(state.amountOfQuestions - 1 > state.questionIndex) {
                commit('setQuestionIndex', state.questionIndex + 1)
            } else {
                commit('setQuestionIndex', 0)
                router.push('/results')
            }
        },
        answerQuestion({ commit }, answer) {
            commit('setAnswer', answer)
        }
    }
})